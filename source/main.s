.section .init

    .globl _start
    _start: MOV sp, #0x8000
            B main

.section .text

    main:   BL InitialiseFrameBuffer
            frameBufferInfo .req r0
            TEQ frameBufferInfo, #0
            BEQ error

            framebufferAddress .req r1
            LDR framebufferAddress, [frameBufferInfo, #32]

            screenWidth .req r2
            LDR screenWidth, [frameBufferInfo, #0]

            colour .req r3
            LDR colour, =0xFFFF

            x .req r4
            y .req r5
            LDR x, =10
            LDR y, =15
            BL DrawRegister10

            .unreq frameBufferInfo
            .unreq framebufferAddress
            .unreq colour

            whileTrue:
            B whileTrue

            error:

                gpioAddress .req r0
                actLed .req r1

                LDR gpioAddress, =0x20200000

                MOV actLed, #1
                LSL actLed, #16
                STR actLed, [gpioAddress, #0x28]            /* GPIO Pin Output Clear 0. */
            
                whileTrue1:
                B whileTrue1

DrawRegister10: PUSH { lr }

                x .req r4
                y .req r5
                testedNumber .req r6

                LDR r10, =0

                registerLoop:

                    MOV testedNumber, #1
                    LSL testedNumber, #31

                    testLoop:

                        TST r10, testedNumber
                        BLEQ Draw0
                        BLNE Draw1
                        ADD x, #5

                        LSR testedNumber, #1
                        TEQ testedNumber, #0

                    BNE testLoop

                    ADD y, #6
                    SUB x, #(32 * 5)

                    ADD r10, #1
                    TEQ r10, #32

                BNE registerLoop

                POP { pc }

DrawPixel:  PUSH { lr } 

            frameBufferInfo .req r0
            framebufferAddress .req r1
            screenWidth .req r2
            colour .req r3
            x .req r4
            y .req r5
            pixelAddress .req r6

            MLA pixelAddress, screenWidth, y, x
            ADD pixelAddress, pixelAddress
            ADD pixelAddress, framebufferAddress

            STRH colour, [pixelAddress]

            .unreq frameBufferInfo
            .unreq framebufferAddress
            .unreq screenWidth
            .unreq colour
            .unreq x
            .unreq y
            .unreq pixelAddress

            POP { pc }

Draw1:  PUSH { r6, lr }

        x .req r4
        y .req r5

        /* Row 0 */
        BL DrawPixel

        ADD x, #1
        BL DrawPixel

        ADD x, #1
        BL DrawPixel

        ADD x, #1
        BL DrawPixel
        /* <- Row 0 */

        /* Row 1 */
        SUB y, #1

        SUB x, #1
        BL DrawPixel
        /* <- Row 1 */

        /* Row 2 */
        SUB y, #1

        BL DrawPixel

        SUB x, #2
        BL DrawPixel
        /* <- Row 2 */

        /* Row 3 */
        SUB y, #1

        ADD x, #1
        BL DrawPixel

        ADD x, #1
        BL DrawPixel
        /* <- Row 3 */

        /* Row 4 */
        SUB y, #1

        BL DrawPixel
        /* <- Row 4 */

        SUB x, #2
        ADD y, #4

        POP { r6, pc }

Draw0:  PUSH { r6, lr }

        x .req r4
        y .req r5

        /* Row 0 */
        BL DrawPixel

        ADD x, #1
        BL DrawPixel

        ADD x, #1
        BL DrawPixel

        ADD x, #1
        BL DrawPixel
        /* <- Row 0 */

        /* Row 1 */
        SUB y, #1

        BL DrawPixel

        SUB x, #3
        BL DrawPixel
        /* <- Row 1 */

        /* Row 2 */
        SUB y, #1

        BL DrawPixel

        ADD x, #3
        BL DrawPixel
        /* <- Row 2 */

        /* Row 3 */
        SUB y, #1

        BL DrawPixel

        SUB x, #3
        BL DrawPixel
        /* <- Row 3 */

        /* Row 3 */
        SUB y, #1

        BL DrawPixel

        ADD x, #1
        BL DrawPixel

        ADD x, #1
        BL DrawPixel

        ADD x, #1
        BL DrawPixel
        /* <- Row 3 */

        SUB x, #3
        ADD y, #4

        POP { r6, pc }
