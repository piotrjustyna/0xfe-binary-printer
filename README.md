# README #

Based on 0xFE, this tiny OS prints contents of register r10 on the screen. r10 gets incremented from 0 to 31 and you can see every value printed in separate line. ***IMPORTANT*** I am not a professional ARM developer and I wrote this project just for fun. Please use the code at your own responsibility. If you spot a bug or something that can be done better, please let me know!